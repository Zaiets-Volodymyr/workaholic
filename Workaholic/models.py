from django.db import models


class Vacancy(models.Model):
    title = models.CharField(max_length=100)
    author_id = models.CharField(max_length=10)
    salary_min = models.IntegerField()
    salary_max = models.IntegerField()
    company = models.CharField(max_length=25)
    city = models.CharField(max_length=20)
    currency = models.CharField(max_length=1, default='₴')
    is_moderated = models.IntegerField(default=0)
    phones = models.CharField(max_length=45)
    logo_link = models.TextField(max_length=1000)
    background_link = models.TextField(max_length=1000)
    description = models.TextField(max_length=10000)

    class Meta:
        db_table = "vacancies"
