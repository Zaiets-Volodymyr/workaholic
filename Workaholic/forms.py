from django import forms
from django.contrib.auth.forms import User

from .models import Vacancy


class VacancyForm(forms.ModelForm):
    class Meta:
        model = Vacancy
        fields = ('title', 'author_id', 'salary_min', 'salary_max', 'company',
                  'city', 'currency', 'is_moderated', 'description')


class UpdateUserForm(forms.ModelForm):
    first_name = forms.CharField(max_length=32, required=False)
    last_name = forms.CharField(max_length=32, required=False)
    email = forms.CharField(max_length=32, required=False)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']


class UpdatePasswordForm(forms.ModelForm):
    password = forms.CharField(max_length=32, required=False)

    class Meta:
        model = User
        fields = ['password']
