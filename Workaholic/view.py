from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, User
from django.contrib.auth import authenticate, login, logout

from .models import Vacancy
from .forms import UpdateUserForm

query = ""


def main(request):
    global query
    if request.method == 'POST':
        query = request.POST['query']
        return redirect("../../../")
    return render(request, 'main.html')


def profile(request, profile_id):
    global query
    try:
        profile_by_id = User.objects.get(id=profile_id)
    except User.DoesNotExist:
        return render(request, '404.html')
    if request.method == 'POST':
        try:
            query = request.POST['query']
        except:
            try:
                if request.POST['old_password'] != "" or request.POST['password'] != "" or \
                        request.POST['password1'] != "":
                    if request.POST['password'] == request.POST['password2'] and \
                            profile_by_id.check_password(request.POST['old_password']):
                        profile_by_id.set_password(request.POST['password'])
                        profile_by_id.save()
                        return redirect("/login")
                    elif not profile_by_id.check_password(request.POST['old_password']):
                        msg = "Wrong old password"
                        return render(request, 'profile.html', {'profile': profile_by_id,
                                                                'msg': msg})
                    else:
                        msg = "Passwords don't match"
                        return render(request, 'profile.html', {'profile': profile_by_id,
                                                                'msg': msg})
            except:
                if profile_by_id.first_name != request.POST['first_name'] or \
                        profile_by_id.last_name != request.POST['last_name'] or \
                        profile_by_id.email != request.POST['email']:
                    form = UpdateUserForm(request.POST, instance=request.user)
                    if form.is_valid():
                        form.save()
                        profile_by_id = User.objects.get(id=profile_id)
                        return render(request, 'profile.html', {'profile': profile_by_id})
                    else:
                        return render(request, 'profile.html', {'profile': profile_by_id})
                else:
                    return render(request, 'profile.html', {'profile': profile_by_id})
        else:
            return redirect("../../../")
    if request.user.id == profile_by_id.id:
        return render(request, 'profile.html', {'profile': profile_by_id})
    else:
        return render(request, '404.html')


def delete_vacancy(request, profile_id):
    global query
    if request.method == 'POST':
        query = request.POST['query']
        return redirect("../../../")
    try:
        profile_by_id = User.objects.get(id=profile_id)
    except User.DoesNotExist:
        return render(request, '404.html')
    else:
        if request.user.id == profile_by_id.id:
            try:
                vacancy_id = request.GET.get('id', "1")
                vacancy_for_delete = Vacancy.objects.filter(author_id=profile_by_id.id, id=vacancy_id)
            except Vacancy.DoesNotExist:
                return render(request, '404.html')
            else:
                vacancy_for_delete.delete()
                return redirect("../vacancies")
        else:
            return render(request, '404.html')


def my_vacancies(request, profile_id):
    global query
    if request.method == 'POST':
        query = request.POST['query']
        return redirect("../../../")
    try:
        profile_by_id = User.objects.get(id=profile_id)
    except User.DoesNotExist:
        return render(request, '404.html')
    else:
        if request.user.id == profile_by_id.id:
            my_vacancies_list = Vacancy.objects.all().filter(author_id=profile_by_id.id)
            vacancies_count = my_vacancies_list.count()
            show_pages = 0
            page = request.GET.get('page', "1")
            if vacancies_count > 10 and page.isnumeric():
                start = int(page) * 10 - 10
                end = int(page) * 10
                max_pages = vacancies_count // 10 + 1
                pages_numbers = [int(page) - 2, int(page) - 1, int(page), int(page) + 1, int(page) + 2, max_pages]
                showed_vacancies = my_vacancies_list[start:end]
                show_pages = 1
                return render(request, 'my_vacancies.html', {'profile': profile_by_id,
                                                             'vacancies': showed_vacancies,
                                                             'vacancies_count': vacancies_count,
                                                             'show_pages': show_pages,
                                                             'pages_numbers': pages_numbers,
                                                             'max_pages': max_pages})
            else:
                return render(request, 'my_vacancies.html', {'vacancies': my_vacancies_list,
                                                             'vacancies_count': vacancies_count,
                                                             'show_pages': show_pages})
        else:
            return render(request, '404.html')


def vacancy(request, vacancy_id):
    global query
    if request.method == 'POST':
        query = request.POST['query']
        return redirect("../../../")
    try:
        vacancy_by_id = Vacancy.objects.get(id=vacancy_id)
        current_user = request.user
        author = User.objects.get(id=vacancy_by_id.author_id)
        phones = []
        for phone in range(vacancy_by_id.phones.__len__() // 15):
            start = phone * 15
            end = phone * 15 + 15
            phone = vacancy_by_id.phones[start:end]
            phones.append(phone)
            print(phone)
    except Vacancy.DoesNotExist:
        return render(request, '404.html')
    else:
        description_lines = vacancy_by_id.description.splitlines()
        return render(request, 'vacancy.html', {'vacancy': vacancy_by_id,
                                                'current_user': current_user,
                                                'author': author,
                                                'phones': phones,
                                                'description_lines': description_lines})


def vacancies(request):
    global query
    if request.method == 'POST':
        query = request.POST['query']
        return redirect("../../../")
    all_vacancies = Vacancy.objects.filter(is_moderated=1)
    filtered_vacancies = []
    for element in all_vacancies:
        if (query.lower() in element.description.lower()) or (query.lower() in element.title.lower()):
            filtered_vacancies.append(element)
    vacancies_count = filtered_vacancies.__len__()
    show_pages = 0
    page = request.GET.get('page', "1")

    if vacancies_count > 10 and page.isnumeric():
        start = int(page) * 10 - 10
        end = int(page) * 10
        max_pages = vacancies_count // 10 + 1
        pages_numbers = [int(page) - 2, int(page) - 1, int(page), int(page) + 1, int(page) + 2, max_pages]
        showed_vacancies = filtered_vacancies[start:end]
        show_pages = 1
        return render(request, 'vacancies.html', {'vacancies': showed_vacancies,
                                                  'query': query,
                                                  'vacancies_count': vacancies_count,
                                                  'show_pages': show_pages,
                                                  'pages_numbers': pages_numbers,
                                                  'max_pages': max_pages})
    else:
        return render(request, 'vacancies.html', {'vacancies': filtered_vacancies,
                                                  'query': query,
                                                  'vacancies_count': vacancies_count,
                                                  'show_pages': show_pages})


def add_vacancy(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            try:
                request.POST['title']
            except:
                global query
                if request.method == 'POST':
                    query = request.POST['query']
                    return redirect("../../../")
                else:
                    query = query
            else:
                if request.POST['currency'] not in {'₴', '$', '€'}:
                    msg = "Wrong currency"
                    return render(request, 'add_vacancy.html', {'msg': msg})
                else:
                    phones = "38-" + request.POST['phone']
                    try:
                        request.POST['phone2']
                    except:
                        phones = phones
                    else:
                        phones += "38-" + request.POST['phone2']
                    try:
                        request.POST['phone3']
                    except:
                        phones = phones
                    else:
                        phones += "38-" + request.POST['phone3']

                    new_vacancy = Vacancy(title=request.POST['title'],
                                          author_id=request.user.id,
                                          salary_min=request.POST['salary_min'],
                                          salary_max=request.POST['salary_max'],
                                          company=request.POST['company'],
                                          city=request.POST['city'],
                                          phones=phones,
                                          currency=request.POST['currency'],
                                          description=request.POST['description'],
                                          logo_link=request.POST['logo_link'],
                                          background_link=request.POST['background_link'])
                    new_vacancy.save()
                    return redirect("/vacancies")
        else:
            return render(request, 'add_vacancy.html')
    else:
        return redirect("../login")


def register(request):
    if request.user.is_authenticated:
        return redirect("/")
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect("/")
        else:
            return render(request, 'register.html', {'form': form})
    else:
        form = UserCreationForm()
        return render(request, 'register.html', {'form': form})


def l_in(request):
    if request.user.is_authenticated:
        return redirect("/")
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("/")
        else:
            msg = 'Wrong Credentials'
            form = UserCreationForm(request.POST)
            return render(request, 'login.html', {'form': form, 'msg': msg})
    else:
        form = UserCreationForm()
        return render(request, 'login.html', {'form': form})


def l_out(request):
    logout(request)
    return redirect("/")


def handler404(request, exception):
    context = {}
    response = render(request, "404.html", context=context)
    response.status_code = 404
    return response


def handler500(request):
    context = {}
    response = render(request, "500.html", context=context)
    response.status_code = 500
    return response
