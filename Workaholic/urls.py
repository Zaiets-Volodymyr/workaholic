"""Workaholic URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
    from django.conf import settings
"""

from django.contrib import admin
from django.urls import path, re_path
from django.views.generic import RedirectView
from django.conf.urls.static import static

from . import settings
from .view import main, register, l_in, l_out, vacancies, add_vacancy, vacancy, profile,\
    my_vacancies, delete_vacancy, handler404, handler500

favicon_view = RedirectView.as_view(url=settings.STATIC_URL + 'drawable/favicon.ico', permanent=True)
admin.autodiscover()

urlpatterns = [
    re_path(r'^favicon\.ico$', favicon_view),
    path('admin/', admin.site.urls),
    path('', main),
    path('register/', register),
    path('login/', l_in),
    path('logout/', l_out),
    path('vacancies/', vacancies),
    path('vacancies/add/', add_vacancy),
    path('vacancy/<int:vacancy_id>/', vacancy),
    path('profile/<int:profile_id>/', profile),
    path('profile/<int:profile_id>/vacancies/', my_vacancies),
    path('profile/<int:profile_id>/delete/', delete_vacancy),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = handler404
handler500 = handler500
